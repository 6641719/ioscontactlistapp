//
//  DataManager.swift
//  PersonsListApp
//
//  Created by Amir on 25.03.2022.
//

class DataManager {
    
    static let shared = DataManager()
    
    let names = ["Amir", "Dimash", "Sultan", "Vitas"]
    let surnames = ["Sairanov", "Sagyntkan", "Ishanov", "Yugai"]
    let emails = ["sun@gmail.com", "snow@gmail.com", "rain@gmail.com", "shadow@gmail.com"]
    let phoneNumbers = ["+7 700 800 90 90", "+7 700 800 70 95", "+7 701 805 92 93", "+7 700 800 85 62"]
    
    private init() {}
}
