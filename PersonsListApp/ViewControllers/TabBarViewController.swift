//
//  TabBarViewController.swift
//  PersonsListApp
//
//  Created by Amir on 25.03.2022.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewControllers()
        // Do any additional setup after loading the view.
    }
    
    private func setupViewControllers() {
        guard let contactListVC = viewControllers?.first as? ContactListViewController else {return}
        guard let sectionVC = viewControllers?.last as? SectionTableViewController else {return}
        
        let persons =  Person.getContactList()
        contactListVC.persons = persons
        sectionVC.persons = persons
    }


}
