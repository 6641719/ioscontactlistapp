//
//  PersonInfoViewController.swift
//  PersonsListApp
//
//  Created by Amir on 25.03.2022.
//

import UIKit

class PersonInfoViewController: UIViewController {
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var person: Person!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = person.fullName
        phoneLabel.text = person.phoneNumber
        emailLabel.text = person.email
    }
    

    

}
